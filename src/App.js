import React from 'react';
import Home from './Router/Home';
import FormHook from './Book/FormHook';

function App() {
  return (
    <div className="App container mt-5">
      <FormHook />
      <hr/>
    </div>
  );
}

export default App;
